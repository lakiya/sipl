package com.sipl.business.manager;

import com.sipl.data.model.User;
import com.sipl.util.ResponseObject;
import org.springframework.stereotype.Component;

/**
 * Created by Lakshitha on 15-Aug-17.
 *
 */
@Component
public interface UserCreationControllerManager {
    ResponseObject saveUser(User user);
}
