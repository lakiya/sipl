package com.sipl.business.manager;

import com.sipl.data.model.auxilary.TenderBulk;
import com.sipl.util.ResponseObject;
import org.springframework.stereotype.Component;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Component
public interface TenderCreationControllerManager {
    ResponseObject saveTenderBulk(TenderBulk tenderBulk);
}
