package com.sipl.business.manager;

import com.sipl.data.model.Tender;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Component
public interface TenderSearchControllerManager {
    List<Tender> searchTenders(Tender tender);

    List<Tender> searchTendersByItemId(Integer itemId);
}
