package com.sipl.business.manager;

import com.sipl.data.model.Company;
import com.sipl.util.ResponseObject;
import org.springframework.stereotype.Component;

/**
 * Created by Lakshitha on 06-Aug-17.
 *
 */
@Component
public interface CompanyCreationControllerManager {
    ResponseObject saveCompany(Company company);

    ResponseObject updateCompany(Company company);
}
