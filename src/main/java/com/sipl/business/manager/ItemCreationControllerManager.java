package com.sipl.business.manager;

import com.sipl.data.model.Item;
import com.sipl.util.ResponseObject;
import org.springframework.stereotype.Component;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Component
public interface ItemCreationControllerManager {
    ResponseObject saveItem(Item item);

    ResponseObject updateItem(Item item);
}
