package com.sipl.business.manager;

import com.sipl.data.model.Location;
import com.sipl.util.ResponseObject;
import org.springframework.stereotype.Component;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Component
public interface LocationCreationControllerManager {
    ResponseObject saveLocation(Location location);

    ResponseObject updateLocation(Location location);
}
