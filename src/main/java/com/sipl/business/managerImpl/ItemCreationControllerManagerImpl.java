package com.sipl.business.managerImpl;

import com.sipl.business.manager.ItemCreationControllerManager;
import com.sipl.data.model.Company;
import com.sipl.data.model.Item;
import com.sipl.data.service.ItemService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Service
public class ItemCreationControllerManagerImpl implements ItemCreationControllerManager {

    private final ItemService itemService;

    @Autowired
    public ItemCreationControllerManagerImpl(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public ResponseObject saveItem(Item item) {
        item = this.itemService.save(item);
        ResponseObject responseObject = new ResponseObject(item, true);
        responseObject.setMessage("Item Saved Successfully");
        return responseObject;
    }

    @Override
    public ResponseObject updateItem(Item item) {
        Item dbItem = this.itemService.findOne(item.getItemId());
        ResponseObject responseObject = new ResponseObject();
        if(dbItem.equals(item)){
            responseObject.setMessage("No Changes found");
            responseObject.setObject(item);
            responseObject.setStatus(false);
        }
        else {
            this.itemService.save(item);
            item = this.itemService.findOne(item.getItemId());
            if(item.getStatus().equals(MasterDataStatus.DELETED.getStatusSeq())){
                responseObject.setMessage("Item Deleted Successfully");
            }
            else {
                responseObject.setMessage("Item Updated Successfully");
            }
            responseObject.setObject(item);
            responseObject.setStatus(true);
        }
        return responseObject;
    }
}
