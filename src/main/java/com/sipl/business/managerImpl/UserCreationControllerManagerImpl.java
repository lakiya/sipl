package com.sipl.business.managerImpl;

import com.sipl.business.manager.UserCreationControllerManager;
import com.sipl.data.model.User;
import com.sipl.data.service.UserService;
import com.sipl.util.ResponseObject;
import com.sipl.util.SHAEncoder;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lakshitha on 15-Aug-17.
 *
 */
@Service
public class UserCreationControllerManagerImpl implements UserCreationControllerManager{

    private final UserService userService;

    @Autowired
    public UserCreationControllerManagerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseObject saveUser(User user) {
        User dbUser = this.userService.findByUsernameAndStatusNot(user.getUsername(), MasterDataStatus.DELETED.getStatusSeq());
        ResponseObject responseObject;
        if(dbUser == null) {
            user.setPassword(SHAEncoder.SHA1(user.getUsername()));
            user = this.userService.save(user);
            responseObject = new ResponseObject(user, true);
            responseObject.setMessage("User Saved Successfully");
        }
        else{
            responseObject = new ResponseObject();
            responseObject.setMessage("Username already taken");
            responseObject.setObject(user);
        }
        return responseObject;
    }
}
