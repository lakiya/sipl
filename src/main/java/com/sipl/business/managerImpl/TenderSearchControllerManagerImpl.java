package com.sipl.business.managerImpl;

import com.querydsl.core.BooleanBuilder;
import com.sipl.business.manager.TenderSearchControllerManager;
import com.sipl.data.model.QTender;
import com.sipl.data.model.Tender;
import com.sipl.data.service.TenderService;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Service
public class TenderSearchControllerManagerImpl implements TenderSearchControllerManager{

    private final TenderService tenderService;

    @Autowired
    public TenderSearchControllerManagerImpl(TenderService tenderService) {
        this.tenderService = tenderService;
    }

    @Override
    public List<Tender> searchTenders(Tender tender) {
        QTender qTender = QTender.tender;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(tender.getItemId() != null && (tender.getItemId() > 0)){
            booleanBuilder.and(qTender.itemId.eq(tender.getItemId()));
        }
        if(tender.getLocationId() != null && (tender.getLocationId() > 0)){
            booleanBuilder.and(qTender.locationId.eq(tender.getLocationId()));
        }
        if(tender.getCompanyId() != null && (tender.getCompanyId() > -1)){
            booleanBuilder.and(qTender.companyId.eq(tender.getCompanyId()));
        }
        booleanBuilder.and(qTender.status.ne(MasterDataStatus.DELETED.getStatusSeq()));
        return (List<Tender>)this.tenderService.findAll(booleanBuilder);
    }

    @Override
    public List<Tender> searchTendersByItemId(Integer itemId) {
        return this.tenderService.findByItemIdAndStatusNot(itemId, MasterDataStatus.DELETED.getStatusSeq());
    }
}
