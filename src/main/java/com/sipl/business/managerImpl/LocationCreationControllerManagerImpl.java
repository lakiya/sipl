package com.sipl.business.managerImpl;

import com.sipl.business.manager.LocationCreationControllerManager;
import com.sipl.data.model.Item;
import com.sipl.data.model.Location;
import com.sipl.data.service.LocationService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Service
public class LocationCreationControllerManagerImpl implements LocationCreationControllerManager {

    private final LocationService locationService;

    @Autowired
    public LocationCreationControllerManagerImpl(LocationService locationService) {
        this.locationService = locationService;
    }

    @Override
    public ResponseObject saveLocation(Location location) {
        location = this.locationService.save(location);
        ResponseObject responseObject = new ResponseObject(location, true);
        responseObject.setMessage("Location Saved Successfully");
        return responseObject;
    }

    @Override
    public ResponseObject updateLocation(Location location) {
        Location dbLocation = this.locationService.findOne(location.getLocationId());
        ResponseObject responseObject = new ResponseObject();
        if(dbLocation.equals(location)){
            responseObject.setMessage("No Changes found");
            responseObject.setObject(location);
            responseObject.setStatus(false);
        }
        else {
            this.locationService.save(location);
            location = this.locationService.findOne(location.getLocationId());
            if(location.getStatus().equals(MasterDataStatus.DELETED.getStatusSeq())){
                responseObject.setMessage("Location Deleted Successfully");
            }
            else {
                responseObject.setMessage("Location Updated Successfully");
            }
            responseObject.setObject(location);
            responseObject.setStatus(true);
        }
        return responseObject;
    }
}
