package com.sipl.business.managerImpl;

import com.sipl.business.manager.CompanyCreationControllerManager;
import com.sipl.data.model.Company;
import com.sipl.data.service.CompanyService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lakshitha on 06-Aug-17.
 *
 */
@Service
public class CompanyCreationControllerManagerImpl implements CompanyCreationControllerManager {

    private final CompanyService companyService;

    @Autowired
    public CompanyCreationControllerManagerImpl(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Override
    public ResponseObject saveCompany(Company company) {
        company = this.companyService.save(company);
        ResponseObject responseObject = new ResponseObject(company, true);
        responseObject.setMessage("Company Saved Successfully");
        return responseObject;
    }

    @Override
    public ResponseObject updateCompany(Company company) {
        Company dbCompany = this.companyService.findOne(company.getCompanyId());
        ResponseObject responseObject = new ResponseObject();
        if(dbCompany.equals(company)){
            responseObject.setMessage("No Changes found");
            responseObject.setObject(dbCompany);
            responseObject.setStatus(false);
        }
        else {
            this.companyService.save(company);
            company = this.companyService.findOne(company.getCompanyId());
            if(company.getStatus().equals(MasterDataStatus.DELETED.getStatusSeq())){
                responseObject.setMessage("Company Deleted Successfully");
            }
            else {
                responseObject.setMessage("Company Updated Successfully");
            }
            responseObject.setObject(company);
            responseObject.setStatus(true);
        }
        return responseObject;
    }
}
