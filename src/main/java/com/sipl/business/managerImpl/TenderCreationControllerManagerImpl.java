package com.sipl.business.managerImpl;

import com.sipl.business.manager.TenderCreationControllerManager;
import com.sipl.data.model.Tender;
import com.sipl.data.model.auxilary.TenderBulk;
import com.sipl.data.model.auxilary.TenderCompanyAux;
import com.sipl.data.service.TenderService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Service
public class TenderCreationControllerManagerImpl implements TenderCreationControllerManager {

    private final TenderService tenderService;

    @Autowired
    public TenderCreationControllerManagerImpl(TenderService tenderService) {
        this.tenderService = tenderService;
    }

    @Override
    public ResponseObject saveTenderBulk(TenderBulk tenderBulk) {
        Tender tender;
        for(TenderCompanyAux tenderCompanyAux : tenderBulk.getTenderCompanyAuxList()){
            tender = new Tender();
            tender.setItemId(tenderCompanyAux.getItemId());
            tender.setLocationId(tenderBulk.getLocationId());
            tender.setCompanyId(tenderBulk.getCompanyId());
            tender.setValue(tenderCompanyAux.getValue());
            tender.setStatus(MasterDataStatus.OPEN.getStatusSeq());
            this.tenderService.save(tender);
        }
        ResponseObject responseObject = new ResponseObject();
        responseObject.setStatus(true);
        responseObject.setMessage("Tender data saved successfully");
        return responseObject;
    }
}
