package com.sipl.data.service;

import com.sipl.data.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
public interface LocationService extends JpaRepository<Location, Integer> {
    List<Location> findByStatusNot(Integer status);
}
