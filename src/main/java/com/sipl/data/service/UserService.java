package com.sipl.data.service;

import com.sipl.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lakshitha on 15-Aug-17.
 *
 */
public interface UserService extends JpaRepository<User,Integer>{
    User findByUsernameAndStatusNot(String username, Integer status);
}
