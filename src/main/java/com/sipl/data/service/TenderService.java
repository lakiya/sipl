package com.sipl.data.service;

import com.sipl.data.model.Tender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Repository
public interface TenderService extends JpaRepository<Tender, Integer> , QueryDslPredicateExecutor<Tender> {

    List<Tender> findByItemIdAndStatusNot(Integer itemId, Integer status);
}
