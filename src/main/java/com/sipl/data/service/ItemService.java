package com.sipl.data.service;

import com.sipl.data.model.Item;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Repository
public interface ItemService extends JpaRepository<Item, Integer> {
    List<Item> findByStatusNot(Integer status);

    List<Item> findByFrequentlyUsedAndStatusNot(Integer frequentlyUsed, Integer status);
}
