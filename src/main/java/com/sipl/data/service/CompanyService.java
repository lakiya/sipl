package com.sipl.data.service;

import com.sipl.data.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lakshitha on 06-Aug-17.
 *
 */
@Repository
public interface CompanyService extends JpaRepository<Company, Integer> {
    List<Company> findByStatusNot(Integer status);
}
