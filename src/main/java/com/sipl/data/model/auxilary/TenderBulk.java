package com.sipl.data.model.auxilary;

import java.util.List;
import java.util.Objects;

/**
 * Created by Lakshitha on 08-Aug-17.
 *
 */
public class TenderBulk {
    private Integer companyId;
    private Integer locationId;
    private List<TenderCompanyAux> tenderCompanyAuxList;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public List<TenderCompanyAux> getTenderCompanyAuxList() {
        return tenderCompanyAuxList;
    }

    public void setTenderCompanyAuxList(List<TenderCompanyAux> tenderCompanyAuxList) {
        this.tenderCompanyAuxList = tenderCompanyAuxList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TenderBulk that = (TenderBulk) o;
        return Objects.equals(companyId, that.companyId) &&
                Objects.equals(locationId, that.locationId) &&
                Objects.equals(tenderCompanyAuxList, that.tenderCompanyAuxList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId, locationId, tenderCompanyAuxList);
    }
}
