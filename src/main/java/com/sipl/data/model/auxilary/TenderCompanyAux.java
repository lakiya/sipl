package com.sipl.data.model.auxilary;

import java.util.Objects;

/**
 * Created by Lakshitha on 08-Aug-17.
 *
 */
public class TenderCompanyAux {
    private Integer itemId;
    private Double value;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TenderCompanyAux that = (TenderCompanyAux) o;
        return Objects.equals(itemId, that.itemId) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, value);
    }
}
