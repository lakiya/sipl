package com.sipl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

/**
 * Created by lakshithar on 6/18/2017.
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final AuthSuccessHandler authSuccessHandler;
    private final AuthLogoutHandler authLogoutHandler;
    private final DataSource dataSource;

    @Autowired
    public SecurityConfig(AuthSuccessHandler authSuccessHandler,
                          AuthLogoutHandler authLogoutHandler,
                          DataSource dataSource) {
        this.authSuccessHandler = authSuccessHandler;
        this.authLogoutHandler = authLogoutHandler;
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/theme/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .requestCache().requestCache(new NullRequestCache()).and()
                .formLogin().loginPage("/login")
                .usernameParameter("username").passwordParameter("password").successHandler(authSuccessHandler)
                .and().rememberMe().rememberMeParameter("remember-me").rememberMeCookieName("my-remember-me").tokenValiditySeconds(86400)
                .and()
                .logout().invalidateHttpSession(true).deleteCookies("JSESSIONID")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login")
                .logoutSuccessHandler(authLogoutHandler)
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")
                .and()
                .csrf();

        http.headers().cacheControl().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(
                        "SELECT USERNAME, PASSWORD, TRUE FROM user WHERE USERNAME=?")
                .passwordEncoder(new ShaPasswordEncoder(1))
                .authoritiesByUsernameQuery(
                        "SELECT USERNAME,TYPE FROM user WHERE USERNAME=?"
                );
    }
}

