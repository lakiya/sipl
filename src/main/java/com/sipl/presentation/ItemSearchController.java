package com.sipl.presentation;

import com.sipl.data.service.ItemService;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("itemSearch")
public class ItemSearchController {

    private final ItemService itemService;

    @Autowired
    public ItemSearchController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String getPage(Model model){
        model.addAttribute("itemList", this.itemService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        return "itemSearch";
    }
}
