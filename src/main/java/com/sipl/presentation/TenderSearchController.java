package com.sipl.presentation;

import com.sipl.business.manager.TenderSearchControllerManager;
import com.sipl.data.model.Tender;
import com.sipl.data.service.CompanyService;
import com.sipl.data.service.ItemService;
import com.sipl.data.service.LocationService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
@Controller
@RequestMapping("tenderSearch")
public class TenderSearchController {

    private final ItemService itemService;
    private final LocationService locationService;
    private final CompanyService companyService;
    private final TenderSearchControllerManager tenderSearchControllerManager;

    @Autowired
    public TenderSearchController(ItemService itemService,
                                  LocationService locationService,
                                  CompanyService companyService,
                                  TenderSearchControllerManager tenderSearchControllerManager) {
        this.itemService = itemService;
        this.locationService = locationService;
        this.companyService = companyService;
        this.tenderSearchControllerManager = tenderSearchControllerManager;
    }

    @GetMapping
    public String getPage(Model model){
        this.setModelData(model);
        return "tenderSearch";
    }

    @RequestMapping(params = "itemId", method = RequestMethod.GET)
    public String loadPage(@RequestParam("itemId") Integer itemId, Model model){
        Tender tender = new Tender();
        tender.setItemId(itemId);
        List<Tender> tenderList = this.tenderSearchControllerManager.searchTendersByItemId(itemId);
        model.addAttribute("tender", tender);
        model.addAttribute("tenderList", tenderList);
        this.setModelData(model);
        return "tenderSearch";
    }


    @RequestMapping(value = "search", method = RequestMethod.POST)
    public String save(@ModelAttribute Tender tender, Model model) {
        List<Tender> tenderList = this.tenderSearchControllerManager.searchTenders(tender);
        model.addAttribute("tender", tender);
        model.addAttribute("tenderList", tenderList);
        this.setModelData(model);
        return "tenderSearch";
    }

    private void setModelData(Model model){
        model.addAttribute("itemList", this.itemService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        model.addAttribute("locationList", this.locationService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        model.addAttribute("companyList", this.companyService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
    }
}
