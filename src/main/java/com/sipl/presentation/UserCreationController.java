package com.sipl.presentation;

import com.sipl.business.manager.UserCreationControllerManager;
import com.sipl.data.model.Location;
import com.sipl.data.model.User;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import com.sipl.util.enums.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Lakshitha on 15-Aug-17.
 *
 */
@Controller
@RequestMapping("user")
public class UserCreationController {

    private final UserCreationControllerManager userCreationControllerManager;

    @Autowired
    public UserCreationController(UserCreationControllerManager userCreationControllerManager) {
        this.userCreationControllerManager = userCreationControllerManager;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String viewPage(Model model){
        this.setModelData(model);
        return "userCreation";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String save(@ModelAttribute User user, Model model) {
        ResponseObject responseObject = this.userCreationControllerManager.saveUser(user);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "userCreation";
    }

    private void setModelData(Model model){
        MasterDataStatus[] statusList = MasterDataStatus.values();
        Arrays.sort(statusList, Collections.reverseOrder());
        model.addAttribute("statusList", statusList);
        model.addAttribute("permissionLevelList", PermissionLevel.values());
    }

    private void setResponseData(ResponseObject responseObject, Model model){
        model.addAttribute("user", responseObject.getObject());
        model.addAttribute("message", responseObject.getMessage());
        model.addAttribute("status", responseObject.getStatus());
    }
}
