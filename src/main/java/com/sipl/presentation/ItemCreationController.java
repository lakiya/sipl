package com.sipl.presentation;

import com.sipl.business.manager.ItemCreationControllerManager;
import com.sipl.data.model.Company;
import com.sipl.data.model.Item;
import com.sipl.data.service.ItemService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import com.sipl.util.enums.YesOrNo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("item")
public class ItemCreationController {

    private final ItemCreationControllerManager itemCreationControllerManager;
    private final ItemService itemService;

    @Autowired
    public ItemCreationController(ItemCreationControllerManager itemCreationControllerManager,
                                  ItemService itemService) {
        this.itemCreationControllerManager = itemCreationControllerManager;
        this.itemService = itemService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String viewPage(Model model){
        this.setModelData(model);
        return "itemCreation";
    }

    @RequestMapping(params = "itemId", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('Admin')")
    public String loadPage(@RequestParam("itemId") Integer itemId, Model model){
        model.addAttribute("item", this.itemService.findOne(itemId));
        this.setModelData(model);
        return "itemCreation";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String save(@ModelAttribute Item item, Model model) {
        ResponseObject responseObject = this.itemCreationControllerManager.saveItem(item);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "itemCreation";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String update(@ModelAttribute Item item, Model model) {
        ResponseObject responseObject = this.itemCreationControllerManager.updateItem(item);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "itemCreation";
    }

    private void setModelData(Model model){
        MasterDataStatus [] statusList = MasterDataStatus.values();
        Arrays.sort(statusList, Collections.reverseOrder());
        model.addAttribute("statusList", statusList);
        model.addAttribute("yesOrNoList", YesOrNo.values());
    }

    private void setResponseData(ResponseObject responseObject, Model model){
        model.addAttribute("company", responseObject.getObject());
        model.addAttribute("message", responseObject.getMessage());
        model.addAttribute("status", responseObject.getStatus());
    }
}
