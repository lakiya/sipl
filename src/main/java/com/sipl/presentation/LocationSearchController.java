package com.sipl.presentation;

import com.sipl.data.service.LocationService;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("locationSearch")
public class LocationSearchController {

    private final LocationService locationService;

    @Autowired
    public LocationSearchController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String getPage(Model model){
        model.addAttribute("locationList", this.locationService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        return "locationSearch";
    }
}
