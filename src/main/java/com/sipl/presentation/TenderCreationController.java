package com.sipl.presentation;

import com.sipl.business.manager.TenderCreationControllerManager;
import com.sipl.data.model.auxilary.TenderBulk;
import com.sipl.data.service.CompanyService;
import com.sipl.data.service.ItemService;
import com.sipl.data.service.LocationService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("tender")
public class TenderCreationController {

    private final ItemService itemService;
    private final LocationService locationService;
    private final CompanyService companyService;
    private final TenderCreationControllerManager tenderCreationControllerManager;

    @Autowired
    public TenderCreationController(ItemService itemService,
                                    LocationService locationService,
                                    CompanyService companyService,
                                    TenderCreationControllerManager tenderCreationControllerManager) {
        this.itemService = itemService;
        this.locationService = locationService;
        this.companyService = companyService;
        this.tenderCreationControllerManager = tenderCreationControllerManager;
    }

    @GetMapping
    public String viewPage(Model model){
        this.setModelData(model);
        model.addAttribute("tenderBulk", new TenderBulk());
        return "tenderCreation";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(@ModelAttribute TenderBulk tenderBulk, Model model) {
        ResponseObject responseObject = this.tenderCreationControllerManager.saveTenderBulk(tenderBulk);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "tenderCreation";
    }

    private void setModelData(Model model){
        model.addAttribute("itemList", this.itemService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        model.addAttribute("locationList", this.locationService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        model.addAttribute("companyList", this.companyService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
    }

    private void setResponseData(ResponseObject responseObject, Model model){
        model.addAttribute("tender", responseObject.getObject());
        model.addAttribute("message", responseObject.getMessage());
        model.addAttribute("status", responseObject.getStatus());
    }
}
