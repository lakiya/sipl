package com.sipl.presentation;

import com.sipl.business.manager.CompanyCreationControllerManager;
import com.sipl.data.model.Company;
import com.sipl.data.service.CompanyService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Lakshitha on 06-Aug-17.
 *
 */
@Controller
@RequestMapping("company")
public class CompanyCreationController {

    private final CompanyCreationControllerManager companyCreationControllerManager;
    private final CompanyService companyService;

    @Autowired
    public CompanyCreationController(CompanyCreationControllerManager companyCreationControllerManager,
                                     CompanyService companyService) {
        this.companyCreationControllerManager = companyCreationControllerManager;
        this.companyService = companyService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String viewPage(Model model){
        this.setModelData(model);
        return "companyCreation";
    }

    @RequestMapping(params = "companyId", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('Admin')")
    public String loadPage(@RequestParam("companyId") Integer companyId, Model model){
        model.addAttribute("company", this.companyService.findOne(companyId));
        this.setModelData(model);
        return "companyCreation";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String save(@ModelAttribute Company company, Model model) {
        ResponseObject responseObject = this.companyCreationControllerManager.saveCompany(company);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "companyCreation";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String update(@ModelAttribute Company company, Model model) {
        ResponseObject responseObject = this.companyCreationControllerManager.updateCompany(company);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "companyCreation";
    }

    private void setModelData(Model model){
        MasterDataStatus [] statusList = MasterDataStatus.values();
        Arrays.sort(statusList, Collections.reverseOrder());
        model.addAttribute("statusList", statusList);
    }

    private void setResponseData(ResponseObject responseObject, Model model){
        model.addAttribute("company", responseObject.getObject());
        model.addAttribute("message", responseObject.getMessage());
        model.addAttribute("status", responseObject.getStatus());
    }
}
