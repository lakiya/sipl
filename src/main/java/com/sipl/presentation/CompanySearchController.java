package com.sipl.presentation;

import com.sipl.data.service.CompanyService;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("companySearch")
public class CompanySearchController {

    private final CompanyService companyService;

    @Autowired
    public CompanySearchController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String getPage(Model model){
        model.addAttribute("companyList", this.companyService.findByStatusNot(MasterDataStatus.DELETED.getStatusSeq()));
        return "companySearch";
    }
}
