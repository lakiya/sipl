package com.sipl.presentation;

import com.sipl.business.manager.LocationCreationControllerManager;
import com.sipl.data.model.Location;
import com.sipl.data.service.LocationService;
import com.sipl.util.ResponseObject;
import com.sipl.util.enums.MasterDataStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
@Controller
@RequestMapping("location")
public class LocationCreationController {

    private final LocationCreationControllerManager locationCreationControllerManager;
    private final LocationService locationService;

    @Autowired
    public LocationCreationController(LocationCreationControllerManager locationCreationControllerManager,
                                      LocationService locationService) {
        this.locationCreationControllerManager = locationCreationControllerManager;
        this.locationService = locationService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Admin')")
    public String viewPage(Model model){
        this.setModelData(model);
        return "locationCreation";
    }

    @RequestMapping(params = "locationId", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('Admin')")
    public String loadPage(@RequestParam("locationId") Integer locationId, Model model){
        model.addAttribute("location", this.locationService.findOne(locationId));
        this.setModelData(model);
        return "locationCreation";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String save(@ModelAttribute Location location, Model model) {
        ResponseObject responseObject = this.locationCreationControllerManager.saveLocation(location);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "locationCreation";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('Admin')")
    public String update(@ModelAttribute Location location, Model model) {
        ResponseObject responseObject = this.locationCreationControllerManager.updateLocation(location);
        this.setModelData(model);
        this.setResponseData(responseObject, model);
        return "locationCreation";
    }

    private void setModelData(Model model){
        MasterDataStatus[] statusList = MasterDataStatus.values();
        Arrays.sort(statusList, Collections.reverseOrder());
        model.addAttribute("statusList", statusList);
    }

    private void setResponseData(ResponseObject responseObject, Model model){
        model.addAttribute("company", responseObject.getObject());
        model.addAttribute("message", responseObject.getMessage());
        model.addAttribute("status", responseObject.getStatus());
    }
}
