package com.sipl.util.enums;

import java.util.Arrays;

/**
 * Created by Lakshitha on 15-Aug-17.
 *
 */
public enum PermissionLevel {
    ADMIN("Admin"),
    USER("User");

    private final String permissionLevel;

    public String getPermissionLevel() {
        return permissionLevel;
    }

    PermissionLevel(String permissionLevel) {
        this.permissionLevel = permissionLevel;
    }
}
