package com.sipl.util.enums;

import java.util.Arrays;

/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
public enum YesOrNo {
    NO(0, "No"),
    Yes(1, "Yes");

    private final Integer yesOrNoSeq;
    private final String yesOrNo;

    YesOrNo(Integer yesOrNoSeq, String yesOrNo) {
        this.yesOrNoSeq = yesOrNoSeq;
        this.yesOrNo = yesOrNo;
    }

    public Integer getYesOrNoSeq() {
        return yesOrNoSeq;
    }

    public String getYesOrNo() {
        return yesOrNo;
    }

    public static YesOrNo findOne(Integer yesOrNoSeq){
        return Arrays.stream(YesOrNo.values())
                .filter(i -> i.getYesOrNoSeq().equals(yesOrNoSeq))
                .findFirst()
                .orElse(null);
    }
}
