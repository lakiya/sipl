package com.sipl.util;

/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
public class ResponseObject {
    private Object object;
    private String message;
    private Boolean status;

    public ResponseObject(String message, Boolean status){
        this.message = message;
        this.status = status;
    }

    public ResponseObject(Object object, Boolean status){
        this.object = object;
        this.status = status;
    }

    public ResponseObject() {
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
