package com.sipl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Lakshitha on 01-Aug-17.
 *
 */
@Service
public class AuthSuccessHandler implements AuthenticationSuccessHandler {

    private final ServletContext servletContext;

    @Autowired
    public AuthSuccessHandler(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.sendRedirect(servletContext.getContextPath());
    }
}
