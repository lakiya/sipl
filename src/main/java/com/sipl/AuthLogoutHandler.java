package com.sipl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Lakshitha on 01-Aug-17.
 *
 */
@Service
public class AuthLogoutHandler implements LogoutSuccessHandler {

    private final ServletContext servletContext;

    @Autowired
    public AuthLogoutHandler(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletResponse.sendRedirect(servletContext.getContextPath() + "/login");
    }
}
