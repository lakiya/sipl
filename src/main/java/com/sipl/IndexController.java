package com.sipl;

import com.sipl.data.model.Item;
import com.sipl.data.service.ItemService;
import com.sipl.util.enums.MasterDataStatus;
import com.sipl.util.enums.YesOrNo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Lakshitha on 01-Aug-17.
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {

    private final ItemService itemService;

    @Autowired
    public IndexController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping
    public String getPage(Model model){
        model.addAttribute("itemList", this.itemService.findByFrequentlyUsedAndStatusNot(YesOrNo.Yes.getYesOrNoSeq(), MasterDataStatus.DELETED.getStatusSeq()));
        return "dashboard";
    }

    @RequestMapping(value = "/findItemData", method = RequestMethod.GET)
    @ResponseBody
    public Item findCurrency(@RequestParam("itemId") Integer itemId) {
        return this.itemService.findOne(itemId);
    }
}
