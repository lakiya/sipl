<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 07-Aug-17
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/dashboard.js"></script>
<div id="page-wrapper">
    <h1 class="page-header">
        Frequently Used
    </h1>
    <div class="container-fluid">
        <c:forEach items="${itemList}" var="item">
            <span onclick="load_item(${item.itemId})" class="badge">${item.itemName}</span>
        </c:forEach>
    </div>
</div>
