<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 01-Aug-17
  Time: 9:23 PM
  To change this template use File | Settings | File Templates.
--%>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">SIPL ERP</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <sec:authentication
                    property="name"/> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <sec:authorize access="hasAuthority('Admin')">
                        <a href="${pageContext.request.contextPath}/user"><i class="fa fa-fw fa-user"></i>New User</a>
                    </sec:authorize>
                    <a href="${pageContext.request.contextPath}/changePassword"><i class="fa fa-fw fa-key"></i> Profile</a>
                    <a href="${pageContext.request.contextPath}/logout"><i class="fa fa-fw fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active navLink">
                <a href="${pageContext.request.contextPath}/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardCompany" class="navLink">
                    <a href="${pageContext.request.contextPath}/company">Company</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardCompanySearch" class="navLink">
                    <a href="${pageContext.request.contextPath}/companySearch">Search Company</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardItem" class="navLink">
                    <a href="${pageContext.request.contextPath}/item">Item</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardItemSearch" class="navLink">
                    <a href="${pageContext.request.contextPath}/itemSearch">Search Item</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardLocation" class="navLink">
                    <a href="${pageContext.request.contextPath}/location">Location</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin')">
                <li id="dashboardLocationSearch" class="navLink">
                    <a href="${pageContext.request.contextPath}/locationSearch">Location Search</a>
                </li>
            </sec:authorize>
            <li id="dashboardTender" class="navLink">
                <a style="color: red" href="${pageContext.request.contextPath}/tender">Tender</a>
            </li>
            <li id="dashboardTenderSearch" class="navLink">
                <a style="color: red" href="${pageContext.request.contextPath}/tenderSearch">Search</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>