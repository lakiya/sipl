<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 01-Aug-17
  Time: 9:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="include.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Lakshitha">

    <title>SIPL ERP</title>

    <link href="${pageContext.request.contextPath}/theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/theme/css/sb-admin.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/theme/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/theme/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/theme/css/bootstrap-select.min.css">

    <script src="${pageContext.request.contextPath}/theme/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/theme/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/script/operations.js" type="text/javascript"></script>
    <script type='text/javascript' src='${pageContext.request.contextPath}/theme/js/noty/jquery.noty.js'></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/tableExport.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/jquery.base64.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/html2canvas.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/jspdf/jspdf.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/theme/js/tableexport/jspdf/libs/base64.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="${pageContext.request.contextPath}/theme/js/bootstrap-select.min.js"></script>
</head>
<body>
<div id="wrapper">
<div id="header">
    <input type="hidden" value="${pageContext.request.contextPath}" id="contextPath">
    <t:insertAttribute name="header"/>
</div>
<div id="content">
    <t:insertAttribute name="body"/>
</div>
</div>
</body>
</html>