<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 01-Aug-17
  Time: 9:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>SIPL ERP</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/theme/css/style.css">
</head>

<body>
<div class="title">
    <img src="theme/img/logo.png"/>
</div>
<!-- Form Module-->
<div class="module form-module">
    <div class="toggle"></div>
    <div class="form">
        <h2>Login to your account</h2>
        <form action="${pageContext.request.contextPath}/login" method="post">
            <input type="text" name="username" placeholder="Username" />
            <input type="password" name="password" placeholder="Password" />
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <button>Login</button>
        </form>
    </div>
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<%--<script src="${pageContext.request.contextPath}/theme/js/index.js"></script>--%>
</body>

</html>