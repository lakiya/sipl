<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 06-Aug-17
  Time: 9:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/companyCreation.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Company
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Company Creation
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                <form action="${pageContext.request.contextPath}/company/save" id="companyCreationForm" method="post">
                    <input type="hidden" name="companyId" value="${company.companyId eq null ? '' : company.companyId}" id="companyId"/>
                    <input type="hidden" value="${message}" id="message"/>
                    <input type="hidden" value="${status}" id="status" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <label>Company Name</label>
                        <input required class="form-control" name="companyName" placeholder="Company Name" value="${company.companyName}">
                    </div>

                    <div class="form-group updateOperation" style="display: none">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <c:forEach items="${statusList}" var="status">
                                <option ${status.statusSeq eq company.status ? 'selected' : ''}
                                              value="${status.statusSeq}">
                                        ${status.status}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row form-group updateOperation" style="display: none;">
                        <hr/>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedBy">Last Modified By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedBy">${company.lastModifiedBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedDate">Last Modified Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedDate"><fmt:formatDate value="${company.lastModifiedDate}"
                                                                             pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdBy">Created By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdBy">${company.createdBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdDate">Created Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdDate"><fmt:formatDate value="${company.createdDate}"
                                                                            pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button onclick="form_validate('companyCreationForm')" type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>