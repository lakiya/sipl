<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 07-Aug-17
  Time: 5:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/tenderCreation.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tender
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Tender Creation
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <form id="tenderInitData">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label for="locationId">Location</label>
                            <select required
                                    id="locationId"
                                    name="locationId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${locationList}" var="location">
                                    <option value="${location.locationId}">${location.locationName}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="companyId">Company</label>
                            <select required
                                    id="companyId"
                                    name="companyId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${companyList}" var="company">
                                    <option value="${company.companyId}">${company.companyName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="itemId">Item</label>
                            <select required
                                    id="itemId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${itemList}" var="item">
                                    <option value="${item.itemId}">${item.itemName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <label for="noOfItems">#</label>
                            <input required class="form-control" id="noOfItems">
                        </div>
                        <div class="col-md-1">
                            <label for="value">Value</label>
                            <input required class="form-control" id="value">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-12">
                            <button onclick="add_row()" type="button" class="btn btn-info">Add</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-12">
                <form action="${pageContext.request.contextPath}/tender/save" method="post" id="tenderForm">
                    <input type="hidden" value="${message}" id="message"/>
                    <input type="hidden" value="${status}" id="status"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="hidden" name="companyId" id="formCompanyId"/>
                    <input type="hidden" name="locationId" id="formLocationId"/>
                    <hr>
                    <table id="tenderData" class="table datatable table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Location</th>
                            <th>Company</th>
                            <th>Item</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="tableBody">

                        </tbody>
                    </table>
                    <hr>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>