<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 15-Aug-17
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/userCreation.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    User
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> User Creation
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                <form action="${pageContext.request.contextPath}/user/save" id="userCreationForm" method="post">
                    <input type="hidden" name="userId" value="${user.userId eq null ? '' : user.userId}" id="userId"/>
                    <input type="hidden" value="${message}" id="message"/>
                    <input type="hidden" value="${status}" id="status" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <label>User Name</label>
                        <input required class="form-control" name="username" placeholder="User Name" value="${user.username}">
                    </div>

                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control">
                            <c:forEach items="${permissionLevelList}" var="permissionLevel">
                                <option ${permissionLevel.permissionLevel eq user.type ? 'selected' : ''}
                                        value="${permissionLevel.permissionLevel}">
                                        ${permissionLevel.permissionLevel}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group updateOperation" style="display: none">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <c:forEach items="${statusList}" var="status">
                                <option ${status.statusSeq eq user.status ? 'selected' : ''}
                                        value="${status.statusSeq}">
                                        ${status.status}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row form-group updateOperation" style="display: none;">
                        <hr/>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedBy">Last Modified By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedBy">${user.lastModifiedBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedDate">Last Modified Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedDate"><fmt:formatDate value="${user.lastModifiedDate}"
                                                                             pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdBy">Created By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdBy">${user.createdBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdDate">Created Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdDate"><fmt:formatDate value="${user.createdDate}"
                                                                            pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button onclick="form_validate('userCreationForm')" type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>