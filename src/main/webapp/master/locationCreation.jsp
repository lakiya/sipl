<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 07-Aug-17
  Time: 5:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/locationCreation.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Location
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Location Creation
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                <form action="${pageContext.request.contextPath}/location/save" id="locationCreationForm" method="post">
                    <input type="hidden" name="locationId" value="${location.locationId eq null ? '' : location.locationId}" id="locationId"/>
                    <input type="hidden" value="${message}" id="message"/>
                    <input type="hidden" value="${status}" id="status" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <label>Location Name</label>
                        <input required class="form-control" name="locationName" placeholder="Location Name" value="${location.locationName}">
                    </div>

                    <div class="form-group updateOperation" style="display: none">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <c:forEach items="${statusList}" var="status">
                                <option ${status.statusSeq eq location.status ? 'selected' : ''}
                                        value="${status.statusSeq}">
                                        ${status.status}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row form-group updateOperation" style="display: none;">
                        <hr/>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedBy">Last Modified By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedBy">${location.lastModifiedBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedDate">Last Modified Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedDate"><fmt:formatDate value="${location.lastModifiedDate}"
                                                                             pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdBy">Created By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdBy">${location.createdBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdDate">Created Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdDate"><fmt:formatDate value="${location.createdDate}"
                                                                            pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button onclick="form_validate('locationCreationForm')" type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
