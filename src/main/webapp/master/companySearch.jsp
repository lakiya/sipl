<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 06-Aug-17
  Time: 9:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/companySearch.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Company Search
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Company Search
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                        Export Data
                    </button>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li><a href="#" onClick="$('#companyData').tableExport({type:'excel',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/xls.png' width="24"/> XLS</a></li>
                        <li><a href="#" onClick="$('#companyData').tableExport({type:'doc',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/word.png' width="24"/> Word</a></li>
                        <li><a href="#" onClick="$('#companyData').tableExport({type:'pdf',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/pdf.png' width="24"/> PDF</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <hr>
                    <table id="companyData" class="table datatable table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Company ID</th>
                            <th>Company Name</th>
                            <th>Created User</th>
                            <th>Created Date</th>
                            <th>Last Modified User</th>
                            <th>Last Modified Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${companyList}" var="company">
                            <tr onclick="load_company(${company.companyId})">
                                <td>${company.companyId}</td>
                                <td>${company.companyName}</td>
                                <td>${company.createdBy}</td>
                                <td><fmt:formatDate value="${company.createdDate}" pattern="dd-MM-yyyy HH:ss"/></td>
                                <td>${company.lastModifiedBy}</td>
                                <td><fmt:formatDate value="${company.lastModifiedDate}" pattern="dd-MM-yyyy HH:ss"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>