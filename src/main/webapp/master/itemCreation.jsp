<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 01-Aug-17
  Time: 9:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/itemCreation.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Item
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Item Creation
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                <form action="${pageContext.request.contextPath}/item/save" id="itemCreationForm" method="post">
                    <input type="hidden" name="itemId" value="${item.itemId eq null ? '' : item.itemId}" id="itemId"/>
                    <input type="hidden" value="${message}" id="message"/>
                    <input type="hidden" value="${status}" id="status" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <label>Item Name</label>
                        <input required class="form-control" name="itemName" placeholder="Item Name" value="${item.itemName}">
                    </div>

                    <div class="form-group">
                        <label>Model</label>
                        <input class="form-control" name="model" placeholder="Item Model" value="${item.model}">
                    </div>

                    <div class="form-group">
                        <label>Frequently Used</label>
                        <select name="frequentlyUsed" class="form-control">
                            <c:forEach items="${yesOrNoList}" var="yesOrNo">
                                <option ${yesOrNo.yesOrNoSeq eq item.frequentlyUsed ? 'selected' : ''}
                                        value="${yesOrNo.yesOrNoSeq}">
                                        ${yesOrNo.yesOrNo}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group updateOperation" style="display: none">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <c:forEach items="${statusList}" var="status">
                                <option ${status.statusSeq eq item.status ? 'selected' : ''}
                                        value="${status.statusSeq}">
                                        ${status.status}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row form-group updateOperation" style="display: none;">
                        <hr/>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedBy">Last Modified By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedBy">${item.lastModifiedBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="modifiedDate">Last Modified Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="modifiedDate"><fmt:formatDate value="${item.lastModifiedDate}"
                                                                             pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdBy">Created By</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdBy">${item.createdBy}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="createdDate">Created Date</label>
                                </div>
                                <div class="col-md-7">
                                    <label id="createdDate"><fmt:formatDate value="${item.createdDate}"
                                                                            pattern="dd-MM-yyyy HH:mm:ss"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button onclick="form_validate('itemCreationForm')" type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>