<%--
  Created by IntelliJ IDEA.
  User: Lakshitha
  Date: 14-Aug-17
  Time: 8:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/layout/include.jsp" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/tenderSearch.js"></script>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tender Search
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="${pageContext.request.contextPath}/">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> Tender Search
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <form action="${pageContext.request.contextPath}/tenderSearch/search" method="post" id="tenderInitData">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-group">
                        <div class="col-md-3">
                            <label for="itemId">Item</label>
                            <select name="itemId"
                                    id="itemId"
                                    name="itemId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${itemList}" var="item">
                                    <option ${item.itemId eq tender.itemId ? 'selected' : ''}
                                            value="${item.itemId}">${item.itemName}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="locationId">Location</label>
                            <select name="locationId"
                                    id="locationId"
                                    name="locationId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${locationList}" var="location">
                                    <option ${location.locationId eq tender.locationId ? 'selected' : ''}
                                            value="${location.locationId}">${location.locationName}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="companyId">Company</label>
                            <select name="companyId"
                                    id="companyId"
                                    class="selectpicker"
                                    data-live-search="true"
                                    title="Select">
                                <c:forEach items="${companyList}" var="company">
                                    <option ${company.companyId eq tender.companyId ? 'selected' : ''}
                                            value="${company.companyId}">${company.companyName}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                        Export Data
                    </button>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li><a href="#" onClick="$('#tenderData').tableExport({type:'excel',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/xls.png' width="24"/> XLS</a></li>
                        <li><a href="#" onClick="$('#tenderData').tableExport({type:'doc',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/word.png' width="24"/> Word</a></li>
                        <li><a href="#" onClick="$('#tenderData').tableExport({type:'pdf',escape:'false'});"><img
                                src='${pageContext.request.contextPath}/theme/img/pdf.png' width="24"/> PDF</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <hr>
                    <table id="tenderData" class="table datatable table-hover table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Location</th>
                            <th>Company</th>
                            <th>Value</th>
                            <th>Created User</th>
                            <th>Created Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${tenderList}" var="tender">
                                <tr>
                                    <td>${tender.item.itemName}</td>
                                    <td>${tender.location.locationName}</td>
                                    <td>${tender.company.companyName}</td>
                                    <td><fmt:formatNumber value="${tender.value}" maxFractionDigits="2" minFractionDigits="2"/></td>
                                    <td>${tender.createdBy}</td>
                                    <td><fmt:formatDate value="${tender.createdDate}" pattern="dd-MM-yyyy HH:ss"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>