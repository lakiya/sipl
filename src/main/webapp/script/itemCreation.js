/**
 * Created by Lakshitha on 07-Aug-17.
 */
$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardItem').addClass('active');
    var itemId = $('#itemId').val();
    if(itemId != ''){
        transform_form('itemCreationForm', 'item/update');
        transformToUpdate();
    }
    display_messages();
});