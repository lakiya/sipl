/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardLocationSearch').addClass('active');
    $('.datatable').dataTable({
        order: [[ 3, "desc" ]],
        dom: "B<'row'<'pull-left'l><'pull-right'f>><'row'<'col-sm-12'tr>><br><'row'<'pull-left'i><'pull-right'p>>",
        language: { search: "_INPUT_", searchPlaceholder: "Search"}
    });
});

function load_location(locationId) {
    var contextPath = $('#contextPath').val();
    window.open(contextPath + '/location?locationId=' + locationId, '_blank');
}