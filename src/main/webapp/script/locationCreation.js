/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardLocation').addClass('active');
    var locationId = $('#locationId').val();
    if(locationId != ''){
        transform_form('locationCreationForm', 'location/update');
        transformToUpdate();
    }
    display_messages();
});