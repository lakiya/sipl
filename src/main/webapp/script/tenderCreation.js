/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardTender').addClass('active');
    $('.selectpicker').selectpicker();
    display_messages();
});

function add_row() {
    var objItemId = $("#itemId");
    var objLocationId = $('#locationId');
    var objCompanyId = $('#companyId');
    var noOfItems = $('#noOfItems').val();

    $('#formCompanyId').val(objCompanyId.val());
    $('#formLocationId').val(objLocationId.val());

    var itemId = objItemId.val();
    var company = objCompanyId.find("option[value='" + objCompanyId.val() + "']").text();
    var item = objItemId.find("option[value='" + objItemId.val() + "']").text();
    var location = objLocationId.find("option[value='" + objLocationId.val() + "']").text();

    var companyId = objCompanyId.find("option[value='" + objCompanyId.val() + "']").val();
    var value = $('#value').val() / noOfItems;
    var html =
        '<tr>' +
        '<td><input readonly class="form-control locationIdClass" value="' + location + '"></td>' +
        '<td><input readonly class="form-control companyIdClass" value="' + company + '"></td>' +
        '<td><input readonly class="form-control itemIdClass" value="' + item + '"></td>' +
        '<td><input readonly class="form-control valueClass" name="tenderCompanyAuxList[0].value" value="' + value + '"></td>' +
        '<td><button type="button" onclick="remove_row($(this))" class="btn btn-danger">-</button></td>' +
        '<input type="hidden" value="' + itemId + '" name="tenderCompanyAuxList[0].itemId"/>' +
        '</tr>';
    $('#tableBody').append(html);
    reset_rows();

}

function reset_rows() {
    var index = 0;
    $("#tableBody").find("tr").each(function () {
        $(this).find(":input").each(function () {
            try {
                if ($(this).attr("name") != null) {
                    $(this).attr("name", $(this).attr("name").replace(/\d+/, index));
                }
                if ($(this).attr("id") != null) {
                    $(this).attr("id", $(this).attr("id").replace(/\d+/, index));
                }
            } catch (e) {
                console.log(e);
            }
        });
        index++;
    });
}

function remove_row(button) {
    var tr = button.closest('tr');
    tr.css("background-color", "#FF3700");
    tr.fadeOut(400, function () {
        tr.remove();
        reset_rows();
    });
}