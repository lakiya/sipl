/**
 * Created by Lakshitha on 14-Aug-17.
 *
 */
$(function () {
    $('.itemIdList').each(function () {
        var id = $(this).val();
        var contextPath = $('#contextPath').val();
        var data = {
            'itemId' : id
        };
        var item = load_object_data(contextPath+'/findItemData', 'GET', data);

        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawMultSeries);

        function drawMultSeries() {
            var data = google.visualization.arrayToDataTable([
                ['Company', 'Value'],
                ['New York City, NY', 8175000],
                ['Los Angeles, CA', 3792000],
                ['Chicago, IL', 2695000],
                ['Houston, TX', 2099000],
                ['Philadelphia, PA', 1526000]
            ]);

            var options = {
                title: item.itemName,
                chartArea: {width: '50%'},
                hAxis: {
                    title: 'Value',
                    minValue: 0
                },
                vAxis: {
                    title: 'Company'
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div['+id+']'));
            chart.draw(data, options);
        }
    });
});


function load_item(itemId) {
    var contextPath = $('#contextPath').val();
    window.open(contextPath + '/tenderSearch?itemId=' + itemId, '_blank');
}