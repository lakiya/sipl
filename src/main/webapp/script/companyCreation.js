/**
 * Created by Lakshitha on 07-Aug-17.
 *
 */
$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardCompany').addClass('active');
    var companyId = $('#companyId').val();
    if(companyId != ''){
        transform_form('companyCreationForm', 'company/update');
        transformToUpdate();
    }
    display_messages();
});