/**
 * Created by Lakshitha on 07-Aug-17.
 * 
 */

$(function () {
    $('.navLink').removeClass('active');
    $('#dashboardCompanySearch').addClass('active');
    $('.datatable').dataTable({
        order: [[ 3, "desc" ]],
        dom: "B<'row'<'pull-left'l><'pull-right'f>><'row'<'col-sm-12'tr>><br><'row'<'pull-left'i><'pull-right'p>>",
        language: { search: "_INPUT_", searchPlaceholder: "Search"}
    });
});

function load_company(companyId) {
    var contextPath = $('#contextPath').val();
    window.open(contextPath + '/company?companyId=' + companyId, '_blank');
}
